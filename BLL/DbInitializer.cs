﻿using DAL;
using DAL.Models;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace BLL
{
    public static class DbInitializer
    {
        public static async Task Initialize(AppDbContext context, UserManager<ClassUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            //context.Database.EnsureDeleted();
            //context.Database.EnsureCreated();

            // Look for any students.
            if (context.Users.Any())
            {
                return;   // DB has been seeded
            }

            var teacherRole = new IdentityRole { Name = "teacher" };
            var userRole = new IdentityRole { Name = "user" };

            // Creating Roles
            await roleManager.CreateAsync(teacherRole);
            await roleManager.CreateAsync(userRole);

            // Creating users
            var admin = new ClassUser { Email = "admin@gmail.com", UserName = "admin@gmail.com" };
            string adminPassword = "P@ssw0rd";
            var adminResult = await userManager.CreateAsync(admin, adminPassword);
            var student = new ClassUser { Email = "student@gmail.com", UserName = "student@gmail.com" };
            string studentPassword = "P@ssw0rd";
            var studentResult = await userManager.CreateAsync(student, studentPassword);

            if (adminResult.Succeeded && studentResult.Succeeded)
            {
                await userManager.AddToRoleAsync(admin, teacherRole.Name);
                await userManager.AddToRoleAsync(admin, userRole.Name);
                await userManager.AddToRoleAsync(student, userRole.Name);
            }
        }
    }
}
