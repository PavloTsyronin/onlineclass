﻿using System;

namespace BLL.DTO
{
    public class WorkDto
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public int? Rate { get; set; }

        public DateTime SubmitTime { get; set; }

        public string StudentId { get; set; }

        public int HomeTaskId { get; set; }

        public string taskText { get; set; }

        public string StudentEmail { get; set; }
    }
}
