﻿using System;

namespace BLL.DTO
{
    public class HomeTaskDto
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public DateTime DeadLine { get; set; }
    }
}
