﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class UserDto
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public bool IsTeacher { get; set; }
    }
}
