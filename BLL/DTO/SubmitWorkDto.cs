﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class SubmitWorkDto
    {
        public string Content { get; set; }

        public string StudentEmail { get; set; }

        public int HomeTaskId { get; set; }
    }
}
