﻿using BLL.DTO;
using BLL.Exceptions;
using BLL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<ClassUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public UserService(UserManager<ClassUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<IdentityResult> Register(string username, string password)
        {
            var newUser = new ClassUser() { UserName = username, Email = username };
            var result = await _userManager.CreateAsync(newUser, password);
            if (!result.Succeeded)
                return result;
            await _userManager.AddToRoleAsync(newUser, "user");
            await _userManager.AddClaimAsync(newUser, new Claim(ClaimTypes.Email, username));
            await _userManager.AddClaimAsync(newUser, new Claim(ClaimTypes.NameIdentifier, newUser.Id));

            return result;
        }

        public async Task MakeTeacher(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user is null)
                throw new UserException("No user with specified email.");
            if (await _userManager.IsInRoleAsync(user, "teacher"))
                throw new UserException("This user is already a teacher.");
            await _userManager.AddToRoleAsync(user, "teacher");
        }

        public async Task<string> GenerateToken(string username, string password)
        {
            //Getting user identity
            var identity = await GetIdentity(username, password);
            if (identity is null)
                throw new UserException("Invalid username or password");

            //Generating JWT
            var encodedJwt = await GenerateToken(identity);

            return encodedJwt;
        }

        /// <summary>
        /// Gets Identity by username and oassword.
        /// </summary>
        /// <param name="username">username.</param>
        /// <param name="password">user's password.</param>
        /// <returns><see cref="IdentityUser"/> object of found user or null.</returns>
        private async Task<ClassUser> GetIdentity(string username, string password)
        {
            if (username is null || password is null)
                return null;
            var identityUser = await _userManager.FindByNameAsync(username);
            if (identityUser != null)
            {
                var result = _userManager.PasswordHasher.VerifyHashedPassword(identityUser, identityUser.PasswordHash, password);
                return result == PasswordVerificationResult.Failed ? null : identityUser;
            }

            return null;
        }


        /// <summary>
        /// Generates JWT for specified user identity.
        /// </summary>
        /// <param name="classUser">User</param>
        /// <returns>Encoded JWT</returns>
        private async Task<string> GenerateToken(ClassUser classUser)
        {
            var userClaims = await GetValidClaims(classUser);
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
            issuer: AuthOptions.ISSUER,
            audience: AuthOptions.AUDIENCE,
            notBefore: now,
            claims: userClaims,
            expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
            signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }


        /// <summary>
        /// Get all the user claims including role claims and claims specific for each role of the user.
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>List of all claims of the user.</returns>
        private async Task<List<Claim>> GetValidClaims(ClassUser user)
        {
            var claims = new List<Claim>();
            var userClaims = await _userManager.GetClaimsAsync(user);
            var userRoles = await _userManager.GetRolesAsync(user);
            claims.AddRange(userClaims);
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
                var role = await _roleManager.FindByNameAsync(userRole);
                if (role != null)
                {
                    var roleClaims = await _roleManager.GetClaimsAsync(role);
                    foreach (Claim roleClaim in roleClaims)
                    {
                        claims.Add(roleClaim);
                    }
                }
            }
            return claims;
        }

        /// <summary>
        /// Get roles of specified of user with specified email.
        /// </summary>
        /// <param name="user">user object.</param>
        /// <returns><see cref="IEnumerable{string}"/> of all roles of the user.</sewe></returns>
        public async Task<IEnumerable<string>> GetUserRoles(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user is null)
                throw new UserException("No user with specified email was found.");
            var userRoles = await _userManager.GetRolesAsync(user);
            return userRoles;
        }

        public IEnumerable<UserDto> GetUsers()
        {
            List<ClassUser> users = _userManager.Users.ToList();

            return users.Select(cu => new UserDto
            {
                Id = cu.Id,
                Email = cu.Email,
                IsTeacher = GetUserRoles(cu.Email).Result.Contains("teacher")
            });
        }
    }
}
