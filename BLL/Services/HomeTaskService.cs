﻿using AutoMapper;
using BLL.DTO;
using BLL.Exceptions;
using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class HomeTaskService : IHomeTaskService
    {
        readonly IUnitOfWork _dataBase;
        readonly IMapper _mapper;

        public HomeTaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _dataBase = unitOfWork;
            _mapper = mapper;
        }

        public async Task<int> CreateAsync(HomeTaskDto homeTaskDto)
        {
            if (homeTaskDto.Id != default(int))
                throw new ModelException("Not allowed to specify the Id of new task.");
            if (homeTaskDto.DeadLine < DateTime.Now)
                throw new ModelException("Task deadline should not be earlier than now.");
            if (String.IsNullOrWhiteSpace(homeTaskDto.Text))
                throw new ModelException("Task text shouldnt consist only of whitespace.");

            var homeTask = _mapper.Map<HomeTask>(homeTaskDto);
            await _dataBase.HomeTaskRepository.AddAsync(homeTask);
            await _dataBase.SaveAsync();
            return homeTask.Id;
        }

        public async Task ModifyAsync(HomeTaskDto homeTaskDto)
        {
            if (homeTaskDto.DeadLine < DateTime.Now)
                throw new ModelException("Task deadline should not be earlier than now.");
            if (String.IsNullOrWhiteSpace(homeTaskDto.Text))
                throw new ModelException("Task text shouldnt consist only of whitespace.");
            if (_dataBase.HomeTaskRepository.GetByIdAsync(homeTaskDto.Id) == null)
                throw new ModelException("No HomeTask with specified Id.");

            var homeTask = _mapper.Map<HomeTask>(homeTaskDto);
            _dataBase.HomeTaskRepository.Update(homeTask);
            await _dataBase.SaveAsync();
        }

        public async Task DeleteAsync(int Id) //TODO: Change to soft delete the task
        {
            var homeTask = await _dataBase.HomeTaskRepository.GetByIdAsync(Id);
            if (homeTask is null)
                throw new ModelException("No HomeTask with such id.");
            _dataBase.HomeTaskRepository.Delete(homeTask);
            await _dataBase.SaveAsync();
        }

        public async Task<HomeTaskDto> GetHomeTaskByIdAsync(int Id)
        {
            var homeTask = await _dataBase.HomeTaskRepository.GetByIdWithDetailsAsync(Id);
            if (homeTask is null)
                throw new ModelException("No HomeTask with such id.");
            return _mapper.Map<HomeTaskDto>(homeTask);
        }

        public IEnumerable<HomeTaskDto> GetAllHomeTasks()
        {
            var homeTasks = _dataBase.HomeTaskRepository.FindAll();
            return homeTasks.Select(ht => _mapper.Map<HomeTaskDto>(ht)).AsEnumerable();
        }
    }
}
