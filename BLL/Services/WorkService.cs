﻿using AutoMapper;
using BLL.DTO;
using BLL.Exceptions;
using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class WorkService : IWorkService
    {
        readonly IUnitOfWork _dataBase;
        readonly IMapper _mapper;
        private readonly UserManager<ClassUser> _userManager;

        public WorkService(IUnitOfWork unitOfWork, IMapper mapper, UserManager<ClassUser> userManager)
        {
            _dataBase = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
        }

        public async Task<int> SubmitWorkAsync(SubmitWorkDto submitDto)
        {
            await ValidateSubmit(submitDto);

            var work = _mapper.Map<Work>(submitDto);
            work.StudentId = (await _userManager.FindByEmailAsync(submitDto.StudentEmail)).Id;
            work.SubmitTime = DateTime.Now;
            await _dataBase.WorkRepository.AddAsync(work);
            await _dataBase.SaveAsync();
            return work.Id;
        }

        public async Task<bool> IsSubmitted(string email, int taskId)
        {
            var student = await _userManager.FindByEmailAsync(email);
            if (student is null)
                return false;
            var studentId = student.Id;

            if (_dataBase.WorkRepository
                .FindAll()
                .Any(w => w.StudentId == studentId
              && w.HomeTaskId == taskId))
            {
                return true;
            }
            return false;
        }


        private async Task ValidateSubmit(SubmitWorkDto submitDto)
        {
            if (String.IsNullOrWhiteSpace(submitDto.Content))
                throw new ModelException("Work shouldnt consist only of whitespace.");
            var student = await _userManager.FindByEmailAsync(submitDto.StudentEmail);
            if (student is null)
                throw new ModelException("There is no student with such id.");

            if (_dataBase.WorkRepository
                    .FindAll()
                    .Any(w => w.StudentId == student.Id
                          && w.HomeTaskId == submitDto.HomeTaskId))
            {
                throw new ModelException("the student already submitted work for this task.");
            }
            var task = await _dataBase.HomeTaskRepository.GetByIdAsync(submitDto.HomeTaskId);
            if (task is null)
                throw new ModelException("There is no task with such id.");
        }

        public async Task RateWorkAsync(int workId, int mark)
        {
            if (mark > 5 || mark < 0)
                throw new ModelException("Mark should be between 0 and 5 points.");
            var work = await _dataBase.WorkRepository.GetByIdAsync(workId);
            if (work is null)
                throw new ModelException("There is no work with such id.");

            work.Rate = mark;
            _dataBase.WorkRepository.Update(work);
            await _dataBase.SaveAsync();
        }

        public async Task<IEnumerable<WorkDto>> GetStudentsWorksAsync(string email)
        {
            var student = await _userManager.FindByEmailAsync(email);
            if (student is null)
                throw new ModelException("There is no student with such email.");

            var works = _dataBase.WorkRepository.GetAllWithDetails()
                        .Where(w => w.Student.Email == email)
                        .OrderByDescending(w => w.SubmitTime)
                        .Select(w => _mapper.Map<WorkDto>(w));
            return works;
        }

        public async Task<IEnumerable<WorkDto>> GetWorksForTaskAsync(int taskId)
        {
            var task = await _dataBase.HomeTaskRepository.GetByIdWithDetailsAsync(taskId);
            if (task is null)
                throw new ModelException("There is no task with such id.");

            return task.Works.Select(w => _mapper.Map<WorkDto>(w));
        }

        public async Task<WorkDto> GetByIdAsync(int Id)
        {
            var work = await _dataBase.WorkRepository.GetByIdAsync(Id);
            if (work is null)
                throw new ModelException("There is no work with such id.");
            return _mapper.Map<WorkDto>(work);
        }

        public async Task<WorkDto> GetByIdWithDetailsAsync(int Id)
        {
            var work = await _dataBase.WorkRepository.GetByIdWithDetailsAsync(Id);
            if (work is null)
                throw new ModelException("There is no work with such id.");
            return _mapper.Map<WorkDto>(work);
        }

    }
}
