﻿using BLL.DTO;
using DAL.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserService
    {
        Task<string> GenerateToken(string username, string password);
        Task<IEnumerable<string>> GetUserRoles(string email);
        Task MakeTeacher(string email);
        Task<IdentityResult> Register(string username, string password);
        IEnumerable<UserDto> GetUsers();
    }
}
