﻿using BLL.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IHomeTaskService
    {
        Task<int> CreateAsync(HomeTaskDto homeTaskDto);
        Task DeleteAsync(int Id);
        IEnumerable<HomeTaskDto> GetAllHomeTasks();
        Task<HomeTaskDto> GetHomeTaskByIdAsync(int Id);
        Task ModifyAsync(HomeTaskDto homeTaskDto);
    }
}
