﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IWorkService
    {
        Task<IEnumerable<WorkDto>> GetStudentsWorksAsync(string email);
        Task<IEnumerable<WorkDto>> GetWorksForTaskAsync(int taskId);
        Task RateWorkAsync(int workId, int mark);
        Task<int> SubmitWorkAsync(SubmitWorkDto submitDto);
        Task<WorkDto> GetByIdAsync(int Id);
        Task<WorkDto> GetByIdWithDetailsAsync(int Id);
        Task<bool> IsSubmitted(string email, int taskId);
    }
}
