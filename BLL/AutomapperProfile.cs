﻿using AutoMapper;
using BLL.DTO;
using DAL.Models;

namespace BLL
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<HomeTask, HomeTaskDto>()
                .ReverseMap();

            CreateMap<Work, WorkDto>()
                .ForMember(wdto => wdto.StudentEmail, x => x.MapFrom(w => w.Student.Email))
                .ForMember(wdto => wdto.taskText, x => x.MapFrom(w => w.HomeTask.Text));

            CreateMap<WorkDto, Work>();

            CreateMap<SubmitWorkDto, Work>();
        }
    }
}
