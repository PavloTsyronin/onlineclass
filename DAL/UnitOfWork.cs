﻿using DAL.Interfaces;
using DAL.Repositories;
using System.Threading.Tasks;

namespace DAL
{
    ///<inheritdoc cref="IUnitOfWork"/>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Context to use.
        /// </summary>
        private readonly AppDbContext _context;

        /// <summary>
        /// Basic constructor.
        /// </summary>
        /// <param name="context">Context to use.</param>
        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }

        private IHomeTaskRepository _homeTaskRepository;
        private IWorkRepository _workRepository;


        public IHomeTaskRepository HomeTaskRepository
        {
            get
            {
                if (_homeTaskRepository is null)
                    _homeTaskRepository = new HomeTaskRepository(_context);
                return _homeTaskRepository;
            }
        }

        public IWorkRepository WorkRepository
        {
            get
            {
                if (_workRepository is null)
                    _workRepository = new WorkRepository(_context);
                return _workRepository;
            }
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
