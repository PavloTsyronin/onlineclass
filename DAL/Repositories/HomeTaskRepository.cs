﻿using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    ///<inheritdoc cref="IHomeTaskRepository"/>
    class HomeTaskRepository : IHomeTaskRepository
    {
        /// <summary>
        /// Context to use.
        /// </summary>
        private readonly AppDbContext _context;

        /// <summary>
        /// Standart constructor.
        /// </summary>
        /// <param name="context">Context to use.</param>
        public HomeTaskRepository(AppDbContext context)
        {
            _context = context;
        }


        public async Task AddAsync(HomeTask entity)
        {
            await _context.HomeTasks.AddAsync(entity);
        }

        public void Delete(HomeTask entity)
        {
            _context.HomeTasks.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            HomeTask homeTask = await _context.HomeTasks.FindAsync(id);
            _context.HomeTasks.Remove(homeTask);
        }

        public IQueryable<HomeTask> FindAll()
        {
            return _context.HomeTasks;
        }

        public IQueryable<HomeTask> FindAllWithDetails()
        {
            return _context.HomeTasks
                .Include(ht => ht.Works);
        }

        public async Task<HomeTask> GetByIdAsync(int id)
        {
            return await _context.HomeTasks.SingleOrDefaultAsync(ht => ht.Id == id);
        }

        public async Task<HomeTask> GetByIdWithDetailsAsync(int id)
        {
            HomeTask homeTask = await _context.HomeTasks.FindAsync(id);
            if (homeTask != null)
            {
                _context.Entry(homeTask).Collection(ht => ht.Works).Load();

                foreach(var work in homeTask.Works)
                {
                    _context.Entry(work).Reference(w => w.HomeTask).Load();
                    _context.Entry(work).Reference(w => w.Student).Load();
                }
            }
            return homeTask;
        }


        public void Update(HomeTask entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
