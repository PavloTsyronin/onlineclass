﻿using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    ///<inheritdoc cref="IWorkRepository"/>
    class WorkRepository : IWorkRepository
    {
        /// <summary>
        /// Context to use.
        /// </summary>
        private readonly AppDbContext _context;

        /// <summary>
        /// Standart constructor.
        /// </summary>
        /// <param name="context">Context to use.</param>
        public WorkRepository(AppDbContext context)
        {
            _context = context;
        }


        public async Task AddAsync(Work entity)
        {
            await _context.Works.AddAsync(entity);
        }

        public void Delete(Work entity)
        {
            _context.Works.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            Work work = await _context.Works.FindAsync(id);
            _context.Works.Remove(work);
        }

        public IQueryable<Work> FindAll()
        {
            return _context.Works;
        }

        public async Task<Work> GetByIdAsync(int id)
        {
            return await _context.Works.FindAsync(id);
        }

        public async Task<Work> GetByIdWithDetailsAsync(int id)
        {
            Work work = await _context.Works.FindAsync(id);
            if (work != null)
            {
                _context.Entry(work).Reference(w => w.Student).Load();
                _context.Entry(work).Reference(w => w.HomeTask).Load();
            }
            return work;
        }

        public void Update(Work entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public IEnumerable<Work> GetAllWithDetails()
        {
            return _context.Works
                .Include(w => w.HomeTask)
                .Include(w => w.Student);
        }
    }
}
