﻿using DAL.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class AppDbContext : IdentityDbContext<ClassUser>
    {
        public AppDbContext(DbContextOptions options) : base(options)
        { 
        
        }

        public virtual DbSet<HomeTask> HomeTasks { get; set; }
        public virtual DbSet<Work> Works { get; set; }
    }
}
