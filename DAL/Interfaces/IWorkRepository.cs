﻿using DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IWorkRepository : IRepository<Work>
    {
        /// <summary>
        /// Asyncronously gets the object with specified Id from context DBSet. Related <see cref="HomeTask"/> objects included.
        /// </summary>
        /// <param name="id">Id of the <see cref="Work"/> object.</param>
        /// <returns><see cref="Task"/> objects containing found object.</returns>
        Task<Work> GetByIdWithDetailsAsync(int id);

        /// <summary>
        /// Gets all <see cref="Work"/> objects from DbContext with details.
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of found objects</returns>
        IEnumerable<Work> GetAllWithDetails();
    }
}
