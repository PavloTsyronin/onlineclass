﻿using DAL.Models;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    /// <summary>
    /// HomeTaskRepository interface.
    /// </summary>
    public interface IHomeTaskRepository : IRepository<HomeTask>
    {
        /// <summary>
        /// Asyncronously gets the object with specified Id from context DBSet. Related <see cref="Work"/> and <see cref="ClassUser"/> objects included.
        /// </summary>
        /// <param name="id">Id of the <see cref="HomeTask"/> object.</param>
        /// <returns><see cref="Task"/> objects containing found object.</returns>
        Task<HomeTask> GetByIdWithDetailsAsync(int id);

        /// <summary>
        /// Finds all <see cref="HomeTask"/> objects with related <see cref="Work"/> and <see cref="ClassUser"/> objects.
        /// </summary>
        /// <returns><see cref="IQueryable"/> of found <see cref="Book"/> objects.</returns>
        IQueryable<HomeTask> FindAllWithDetails();
    }
}
