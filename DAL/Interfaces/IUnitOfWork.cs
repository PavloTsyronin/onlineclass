﻿using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        /// <summary>
        /// Work Repository
        /// </summary>
        IWorkRepository WorkRepository { get; }

        /// <summary>
        /// HomeTask Repository
        /// </summary>
        IHomeTaskRepository HomeTaskRepository { get; }


        /// <summary>
        /// Saves all comitted changes to DBSets.
        /// </summary>
        /// <returns><see cref="Task"/> object containing information about number of added Entities.</returns>
        Task<int> SaveAsync();
    }
}
