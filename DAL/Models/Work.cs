﻿using System;

namespace DAL.Models
{
    public class Work : BaseEntity
    {
        public string Content { get; set; }

        public int? Rate { get; set; }

        public DateTime SubmitTime { get; set; }

        public string StudentId { get; set; }

        public int HomeTaskId { get; set; }


        public virtual ClassUser Student { get; set; }

        public virtual HomeTask HomeTask { get; set; }
    }
}
