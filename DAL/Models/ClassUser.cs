﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace DAL.Models
{
    /// <summary>
    /// Represents OnlineClass user identity.
    /// </summary>
    public class ClassUser : IdentityUser
    {
        public virtual ICollection<Work> Works { get; set; }
    }
}
