﻿using System;
using System.Collections.Generic;

namespace DAL.Models
{
    public class HomeTask : BaseEntity
    {
        public string Text { get; set; }

        public DateTime DeadLine { get; set; }

        public virtual ICollection<Work> Works { get; set; }
    }
}
