﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using BLL.Interfaces;
using ClassWebAPI.Models;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ClassWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }


        [HttpPost("/register")]
        public async Task<IActionResult> Register([FromBody] AuthModel authModel)
        {
            var dictionary = new ModelStateDictionary();

            if (ValidateEmail(authModel.Email))
            {
                var result = await _userService.Register(authModel.Email, authModel.Password);

                if (result.Succeeded)
                    return Ok();

                foreach (IdentityError error in result.Errors)
                {
                    dictionary.AddModelError(error.Code, error.Description);
                }
            }
            else
            {
                dictionary.AddModelError("InvalidEmailAddress", "Invalid email address");
            }
            return new BadRequestObjectResult(new { Message = "User Registration Failed", Errors = dictionary });
        }

        private bool ValidateEmail(string email)
        {
            Regex regex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
                RegexOptions.CultureInvariant | RegexOptions.Singleline);

            return regex.IsMatch(email);
        }

        [HttpPost("/token")]
        public async Task<IActionResult> Token([FromBody] AuthModel authModel)
        {
            string encodedJwt;
            IEnumerable<string> roles;
            encodedJwt = await _userService.GenerateToken(authModel.Email, authModel.Password);
            roles = await _userService.GetUserRoles(authModel.Email);
            var response = new
            {
                access_token = encodedJwt,
                email = authModel.Email,
                roles = roles
            };
            return Json(response);
        }

        [Authorize(Roles = "teacher")]
        [HttpPost("/maketeacher/{email}")]
        public async Task<IActionResult> MakeTeacher(string email)
        {
            await _userService.MakeTeacher(email);
            return Ok();
        }
    }
}
