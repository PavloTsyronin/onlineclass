﻿using BLL.Exceptions;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClassWebAPI.Controllers
{
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ErrorController : ControllerBase
    {
        [Route("/error")]
        public ActionResult Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context?.Error;

            if (exception is ModelException)
                return BadRequest(new { errorText = exception.Message });

            return new JsonResult("Wrong!");
        }

        [Route("/error-local-development")]
        public ActionResult ErrorDev()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            var exception = context?.Error; 

            if (exception is ModelException || exception is UserException)
                return BadRequest(new { errorText = exception.Message });

            return new JsonResult("Wrong!");
        }
    }
}
