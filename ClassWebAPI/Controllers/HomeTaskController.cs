﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ClassWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class HomeTaskController : ControllerBase
    {
        private readonly IHomeTaskService _homeTaskService;

        public HomeTaskController(IHomeTaskService homeTaskService)
        {
            _homeTaskService = homeTaskService;
        }

        [HttpPost]
        [Authorize(Roles = "teacher")]
        public async Task<IActionResult> Create([FromBody] HomeTaskDto homeTaskDto)
        {
            int id = await _homeTaskService.CreateAsync(homeTaskDto);
            var newHomeTask = _homeTaskService.GetHomeTaskByIdAsync(id);
            return CreatedAtAction(nameof(GetById), new { id = id }, newHomeTask);
        }

        [HttpPut]
        [Authorize(Roles = "teacher")]
        public async Task<IActionResult> Modify([FromBody] HomeTaskDto homeTaskDto)
        {
            await _homeTaskService.ModifyAsync(homeTaskDto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "teacher")]
        public async Task<IActionResult> Delete(int id)
        {
            await _homeTaskService.DeleteAsync(id);
            return NoContent();
        }


        [HttpGet("{id}")]
        [Authorize(Roles = "user, teacher")]
        public async Task<ActionResult<HomeTaskDto>> GetById(int id)
        {
            var homeTask = await _homeTaskService.GetHomeTaskByIdAsync(id);
            return homeTask;
        }

        [HttpGet]
        [Authorize(Roles = "user, teacher")]
        public ActionResult<IEnumerable<HomeTaskDto>> GetAll()
        {
            var homeTasks = _homeTaskService.GetAllHomeTasks().ToList();
            return homeTasks;
        }

    }
}
