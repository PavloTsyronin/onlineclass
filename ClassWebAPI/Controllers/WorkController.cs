﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ClassWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class WorkController : ControllerBase
    {
        private readonly IWorkService _workService;

        public WorkController(IWorkService workService)
        {
            _workService = workService;
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Submit([FromBody] SubmitWorkDto submitWorkDto)
        {
            submitWorkDto.StudentEmail = User.FindFirstValue(ClaimTypes.Email);
            var id = await _workService.SubmitWorkAsync(submitWorkDto);
            var workDto = _workService.GetByIdAsync(id);
            return CreatedAtAction(nameof(GetById), new { id = id }, workDto);
        }

        [Authorize]
        [HttpGet("{email}/{taskId}")]
        public async Task<IActionResult> IsSubmitted(string email, int taskId)
        {
            bool res = await _workService.IsSubmitted(email, taskId);
            return Ok(new { IsSubmitted = res });
        }



        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<WorkDto>> GetById(int id)
        {
            var workDto = await _workService.GetByIdWithDetailsAsync(id);
            if (!User.IsInRole("teacher"))
            {
                var userEmail = User.FindFirstValue(ClaimTypes.Email);
                if (workDto.StudentEmail != userEmail)
                    return BadRequest(new { errorText = "No work with such id was submited cu current user" });
            }

            return workDto;
        }

        [HttpPost("{workId}/{mark}")]
        [Authorize(Roles = "teacher")]
        public async Task<IActionResult> Rate(int workId, int mark)
        {
            await _workService.RateWorkAsync(workId, mark);
            return NoContent();
        }

        [HttpGet("myworks")]
        [Authorize]
        public async Task<IEnumerable<WorkDto>> GetMyWorks()
        {
            string studentEmail = User.FindFirstValue(ClaimTypes.Email);
            var works = await _workService.GetStudentsWorksAsync(studentEmail);
            return works;
        }

        [HttpGet("studentsworks/{email}")]
        [Authorize(Roles = "teacher")]
        public async Task<ActionResult<IEnumerable<WorkDto>>> GetStudensWorks(string email)
        {
            var works = await _workService.GetStudentsWorksAsync(email);
            return Ok(works);
        }

        [HttpGet("tasksworks/{taskId}")]
        [Authorize(Roles = "teacher")]
        public async Task<ActionResult<IEnumerable<WorkDto>>> GetTasksWorks(int taskId)
        {
            var works = await _workService.GetWorksForTaskAsync(taskId);
            return Ok(works);
        }

    }
}
